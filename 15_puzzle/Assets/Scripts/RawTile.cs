﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RawTile : MonoBehaviour
{
    [SerializeField] private AnimationCurve animationCurve;
    public int value;
    int size;
    [SerializeField] private RawImage imageHolder;
    [SerializeField] private RectTransform rectCanvas;

    public void SetValue(int val, int pos, Texture image, Transform parent, int size)
    {
        value = val;
        this.size = size;

        transform.SetParent(parent, false);

        imageHolder.texture = image;

        float tileSize = 1.0f / size;

        int yMinForUV = (value - 1) / size;
        int xMinForUV = (value - 1) % size;

        Rect r = new Rect();
        r.width = tileSize;
        r.height = tileSize;
        r.x = tileSize * xMinForUV;
        r.y = 1.0f - tileSize * yMinForUV - tileSize;
        imageHolder.uvRect = r;

        rectCanvas.anchorMax = CalcAnchorsMax(pos, size);
        rectCanvas.anchorMin = CalcAnchorsMin(pos, size);
    }

    public void MoveTo(Vector2 coords)
    {
        Debug.Log(value);
        Debug.Log(coords.ToString());
        rectCanvas.anchorMax = CalcAnchorsMax(Mathf.RoundToInt((coords.x) * size + coords.y+1), size);
        rectCanvas.anchorMin = CalcAnchorsMin(Mathf.RoundToInt((coords.x) * size + coords.y+1), size);
    }

    Vector2 CalcAnchorsMax(int position, int size)
    {
        float tileSize = 1.0f / size;
        int xMax = (position - 1) % size + 1;
        int yMin = (position - 1) / size;
        return new Vector2(tileSize * xMax, 1.0f - tileSize * yMin);
    }

    Vector2 CalcAnchorsMin(int position, int size)
    {
        float tileSize = 1.0f / size;
        int xMin = (position - 1) % size;
        int yMax = (position - 1) / size + 1;
        return new Vector2(tileSize * xMin, 1.0f - tileSize * yMax);
    }

}
