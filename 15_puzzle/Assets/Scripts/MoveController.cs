﻿using UnityEngine;
using System.Collections;

public class MoveController : MonoBehaviour {

    private bool alreadyMoved = false;
    [SerializeField] private GameController gc;
    [SerializeField] private float threshold;
	
	// Update is called once per frame
	void Update ()
    {

        float mov = Mathf.Abs(Input.GetAxis("Horizontal")) > Mathf.Abs(Input.GetAxis("Vertical")) ? Input.GetAxis("Horizontal") : Input.GetAxis("Vertical");
        bool isHorizontal = Mathf.Abs(Input.GetAxis("Horizontal")) > Mathf.Abs(Input.GetAxis("Vertical"));

        if (!alreadyMoved && Mathf.Abs(mov) > threshold)
        {
            GameController.Directions dir;
            if (isHorizontal)
            {
                dir = mov > 0 ? GameController.Directions.Right : GameController.Directions.Left;
            }
            else
            {
                dir = mov > 0 ? GameController.Directions.Up : GameController.Directions.Down;
            }
            gc.MakeMove(dir);
        }

        alreadyMoved = Mathf.Abs(mov) > threshold;
	}
}
