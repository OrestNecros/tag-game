﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameController : MonoBehaviour {

    //TEMP
    public Texture image;
    //
    

    [SerializeField] private RawTile tile;
    [SerializeField] private GameObject tileContainer;

    private RawTile[,] tileMap;
    private int[] values;
    private int size;
    private Vector2 emptyTile;

    public enum Directions { Up = 1, Right = 2, Down = 3, Left = 0}

    public void CreateFieldOfSize(int size)
    {
        tileMap = new RawTile[size, size];
        values = new int[size * size - 1];
        emptyTile = new Vector2(size-1, size-1);
        this.size = size;

        for (int a = 0; a < values.Length; a++)
        {
            values[a] = a + 1;
        }

        do
        {
            RandomizeSetup();
        } while (!CheckIfValid());

        for (int i = 0; i < size; i++)
        {
            for (int x = 0; x < size; x++)
            {
                if ((i * size + x + 1) == size * size)
                {
                    break;
                }
                RawTile thisTile = Instantiate(tile);
                thisTile.SetValue(values[i * size + x], i * size + x + 1, image, tileContainer.transform, size);
                tileMap[i, x] = thisTile;
                if (values[i * size + x] == 0)
                {
                    Debug.Log("Whoa!");
                }
            }
        }
    }

    //we randomise and check if setup is valid
    void RandomizeSetup()
    {
        //shuffle the array
        for (int i = 0; i < values.Length; i++)
        {
            int r = Random.Range(0, i);
            int tmp = values[i];
            values[i] = values[r];
            values[r] = tmp;
        }
    }

    //in theory - there is possibility to create unsolvable puzzles. This should filter them out.
    bool CheckIfValid()
    {
        /*int sum = 0;
        for (int count = 0; count < values.Length; count++)
        {
            for (int secondaryCount = count + 1; secondaryCount < values.Length; secondaryCount++)
            {
                if (values[secondaryCount] < values[count])
                {
                    sum++;
                }
            }
        }
        if (sum % 2 == 1)
        {
            return false;
        }*/
        return true;
    }

    public void MakeMove(Directions dir)
    {
        Debug.Log(dir);
        Vector2 tileToMove = ConvertDirToTile(dir, emptyTile, tileMap);
        int x = (int)tileToMove.x;
        int y = (int)tileToMove.y;
        if (x >= size || y >= size || y < 0 || x < 0)
        {
            return;
        }
        tileMap[x, y].MoveTo(emptyTile);
        tileMap[Mathf.RoundToInt(emptyTile.x), Mathf.RoundToInt(emptyTile.y)] = tileMap[x, y];
        tileMap[x, y] = null;
        emptyTile = new Vector2(x, y);
    }



    static Vector2 ConvertDirToTile(Directions d, Vector2 origin, RawTile[,] map)
    {
        int dir = (int) d;
        if (dir >= 2)
        {
            return new Vector2(origin[0] - (dir % 2), origin[1] - ((dir + 1) % 2)); //a little bit of magic to convert from clock-wise directions into real coordinates
        }
        else
        {
            return new Vector2(origin[0] + (dir % 2), origin[1] + ((dir + 1) % 2));
        }
    }

}
