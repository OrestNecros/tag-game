﻿using UnityEngine;
using System.Collections;

public class MainMenuController : MonoBehaviour {

	public GameObject credits;

	public void StartLimitedTurns(){
		PlayerPrefs.SetInt ("GameMode", 1);
		Application.LoadLevel("Game");
		//UnityEngine.SceneManagement.SceneManager.LoadScene ("Game");
	}
	
	public void StartLimitedTime() {
		PlayerPrefs.SetInt ("GameMode", 2);
		Application.LoadLevel("Game");
		//UnityEngine.SceneManagement.SceneManager.LoadScene ("Game");
	}

	public void ShowCredits(){
		credits.SetActive (true);
	}

	public void HideCredits(){
		credits.SetActive (false);
	}
}
