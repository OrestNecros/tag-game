﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class PuzzleManager : MonoBehaviour
{

	//swipe stuff
	private Vector3 firstPos;
	private Vector3 lastPos;
	private float drag = 10.0f;

	//gameObjects to access
	public Tile genericTile;
	public Text countdown;
	public Text countdownTitle;
	public GameObject victoryPannel;
	public Text victoryText;

	//gameMode variables
	private bool turnLimit = false;
	private bool timeLimit = false;
	private int turnCount = 400;
	private float timeCount = 300.0f;

	//we need map of all tiles (4*4 array of gameObjects or Tiles)
	private Tile[,] tileMap = new Tile [4, 4];
	private int[] values = new int [15];

	//we need to have coordinates of empty tile (as [x,y])
	private Vector2 emptyTile = new Vector2(3,3);

	//generate map
	void Start ()
	{
		if (PlayerPrefs.GetInt ("GameMode") == 1) {
			turnLimit = true;
			countdown.text = turnCount.ToString ();
			countdownTitle.text = "Turns left:";
		} else {
			timeLimit = true;
			countdown.text = timeCount.ToString ();
			countdownTitle.text = "Time left:";
		}

		for (int a = 0; a < 15; a++) {
			values [a] = a + 1;
		}

		do {
			RandomizeSetup ();
		} while (!CheckIfValid());
		
		for (int i = 0; i < 4; i++) {
			for (int x = 0; x < 4; x++) {
				if ((i * 4 + x + 1) == 16) {
					break;
				}
				Tile thisTile = Instantiate (genericTile);
				thisTile.SetInitialValues (values [i * 4 + x], new Vector3 (x, 3 - i), gameObject.transform);
				tileMap [i, x] = thisTile;
			}
		}
	}

	//we randomise and check if setup is valid
	void RandomizeSetup ()
	{
		//shuffle the array
		for (int i = 0; i < values.Length; i++) {
			int r = UnityEngine.Random.Range (0, i);
			int tmp = values [i];
			values [i] = values [r];
			values [r] = tmp;
		}
	}

	//in theory - there is possibility to create unsolvable puzzles. This should filter them out.
	bool CheckIfValid ()
	{
		int sum = 0;
		for (int count = 0; count < values.Length; count ++) {
			for (int secondaryCount = count + 1; secondaryCount < values.Length; secondaryCount++) {
				if (values [secondaryCount] < values [count]) {
					sum ++;
				}
			}
		}
		if (sum % 2 == 1) {
			return false;
		}
		return true;
	}


	//now let`s try to move our tiles
	void TryMoveTile (Vector3 direction)
	{
		if ((!turnLimit) && (!timeLimit)) { //if game is stopped - we don`t want to move tiles any more
			return;
		}
		try {
			Tile checkedTile = tileMap [(int)(emptyTile.x + direction.y), (int)(emptyTile.y - direction.x)];
			checkedTile.MoveTile(direction);
			tileMap [(int)emptyTile.x, (int)emptyTile.y] = checkedTile;
			tileMap [(int)(emptyTile.x + direction.y), (int)(emptyTile.y - direction.x)] = null;
			emptyTile.x = emptyTile.x+direction.y;
			emptyTile.y = emptyTile.y-direction.x;
			Debug.Log(emptyTile.ToString());
			//emptyTile [0] += (int)direction [1];
			//emptyTile [1] -= (int)direction [0];
			if (turnLimit) {
				turnCount--;
				countdown.text = turnCount.ToString ();
			}
			CheckVictoryConditions ();
		} catch (Exception e) {
			Debug.Log ("Inapropriate move!!");
		}
	}

	//check this tile position for win condition
	void CheckVictoryConditions ()
	{
		if ((turnLimit) && (turnCount < 1)) {
			StopGame (false);
		}
		int count = 0;
		foreach (Tile tile in tileMap) {
			count++;
			if (count == 16) {
				break;
			}
			if (tile == null || !tile.CheckValue(count)) {
				return;
			}
		}
		StopGame (true);
	}

	void StopGame (bool victory)
	{
		timeLimit = false;
		turnLimit = false;
		victoryPannel.SetActive (true);

		if (victory) {
			victoryText.text = "You won!";
		} else {
			victoryText.text = "You lost!";
		}
	}

	//try to move tiles with keys
	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.LeftArrow)) {
			TryMoveTile (Vector3.left);
		}
		if (Input.GetKeyDown (KeyCode.RightArrow)) {
			TryMoveTile (Vector3.right);
		}
		if (Input.GetKeyDown (KeyCode.UpArrow)) {
			TryMoveTile (Vector3.up);
		}
		if (Input.GetKeyDown (KeyCode.DownArrow)) {
			TryMoveTile (Vector3.down);
		}

		if (timeLimit) {
			timeCount -= Time.deltaTime;
			countdown.text = Math.Round (timeCount).ToString ();
			if (timeCount < 0) {
				StopGame (false);
			}
		}

		//well, time to add some swipe detection here

		foreach (Touch touch in Input.touches) {
			if (touch.phase == TouchPhase.Began) {
				firstPos = touch.position;
				lastPos = touch.position;
			}

			if (touch.phase == TouchPhase.Moved) {
				lastPos = touch.position;
			}

			if (touch.phase == TouchPhase.Ended) {
				if (Mathf.Abs (lastPos.x - firstPos.x) > drag || Mathf.Abs (lastPos.y - firstPos.y) > drag) { //is it enough?
					if (Mathf.Abs (lastPos.x - firstPos.x) > Mathf.Abs (lastPos.y - firstPos.y)) { //which axis
						if (lastPos.x > firstPos.x) { //right
							TryMoveTile (Vector3.right);
						} else { //left
							TryMoveTile (Vector3.left);
						}
					} else {
						if (lastPos.y > firstPos.y) {//up
							TryMoveTile (Vector3.up);
						} else { //down
							TryMoveTile (Vector3.down);
						}
					}
				}
			}
		}

	}
		

}
