﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour {

	int value;
	public TextMesh valueText;

	public void SetInitialValues(int x, Vector3 newPosition, Transform parent){
		value = x;
		gameObject.transform.position = newPosition;
		gameObject.transform.SetParent(parent);
		valueText.text = value.ToString();	
	}

    public int GetValue() {
        return value;
    }

	public bool CheckValue (int x){
		return (x == value);
	}

	public void MoveTile (Vector3 direction){
		transform.position += direction;
	}
}
