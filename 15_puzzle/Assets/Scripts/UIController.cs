﻿using UnityEngine;
using System.Collections;

public class UIController : MonoBehaviour {

    [SerializeField] private GameObject sizePanel;
    [SerializeField] private GameController gameCanvas;

    public void ShowSizeSelection(bool state)
    {
        sizePanel.SetActive(state);
    }

    public void StartGameOfSize(int size)
    {
        gameCanvas.gameObject.SetActive(true);
        gameCanvas.CreateFieldOfSize(size);
        gameObject.SetActive(false);
    }
}
